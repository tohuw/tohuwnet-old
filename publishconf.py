#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

## Paths (Published)
SITEURL = 'https://tohuw.net'
RELATIVE_URLS = False
DELETE_OUTPUT_DIRECTORY = True

ARTICLE_SAVE_AS = 'articles/{slug}'
ARTICLE_LANG_SAVE_AS = 'articles/{slug}-{lang}'

PAGE_SAVE_AS = '{slug}'
PAGE_LANG_SAVE_AS = '{slug}-{lang}'

DRAFT_URL = ''
DRAFT_SAVE_AS = ''
DRAFT_LANG_URL = ''
DRAFT_LANG_SAVE_AS = ''

CATEGORY_SAVE_AS = 'topic/{slug}'
CATEGORIES_SAVE_AS = 'topics'

TAG_SAVE_AS = 'tag/{slug}'
TAGS_SAVE_AS = 'tags'

ARCHIVES_SAVE_AS = 'archives/index.html'
YEAR_ARCHIVE_SAVE_AS = 'archives/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'archives/{date:%Y}/{date:%b}/index.html'

## Feeds (Published)
FEED_DOMAIN = SITEURL
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'

## Theme-Specific Settings (Published)
ABOUT_SAVE_AS = 'about'
ARTICLES_SAVE_AS = 'articles/index.html'
ARTICLE_SHARE_LINKS = True
# DISQUS_SITENAME = "tohuw"
ISSO_URL = 'https://comments.tohuw.net'
FEEDS_LINK = SITEURL + '/' + FEED_ALL_ATOM
LICENSE_URL = SITEURL + '/rights'
MATOMO_SITE_ID = '1'
MATOMO_URL = 'stats.tohuw.net'
SEARCH_SAVE_AS = 'search'
TIPUE_CONTENT_LOCATION = SITEURL + '/tipuesearch_content.json'
