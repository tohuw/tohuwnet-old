"""
Pelican Configuration
This includes all documented settings as of Pelican 3.7.1
It also includes settings specific to the 'Anew' theme and a few plugins
"""
#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

## Naming & Locale
SITENAME = 'Tohuw.Net'
SITESUBTITLE = 'Clouds, Chaos, & Yak Shaving'
AUTHOR = 'Ron Scott-Adams'
DEFAULT_LANG = 'en'
if os.name == 'nt':
    LOCALE = ('usa')
else:
    LOCALE = ('en_US.UTF-8')
# DEFAULT_DATE = None
DEFAULT_DATE_FORMAT = '%B %d, %Y'
# DATE_FORMATS = {
#     'en': '%B %d, %Y',
#     'jp': '%Y-%m-%d(%a)',
# }
TIMEZONE = 'America/Chicago'

## Paths
SITEURL = 'http://localhost:3000'
RELATIVE_URLS = True
PATH = 'content'
# OUTPUT_PATH = 'output/'
# DELETE_OUTPUT_DIRECTORY = False
# OUTPUT_RETENTION = []
# WRITE_SELECTED = []
ARTICLE_PATHS = ['articles']
# ARTICLE_EXCLUDES = []
ARTICLE_URL = 'articles/{slug}'
ARTICLE_SAVE_AS = 'articles/{slug}.html'
ARTICLE_LANG_URL = 'articles/{slug}-{lang}'
ARTICLE_LANG_SAVE_AS = 'articles/{slug}-{lang}.html'
# INDEX_SAVE_AS = 'index.html'
# ARTICLE_ORDER_BY = 'reversed-date'

# PAGE_PATHS = ['pages']
# PAGE_EXCLUDES = []
PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}.html'
PAGE_LANG_URL = '{slug}-{lang}'
PAGE_LANG_SAVE_AS = '{slug}-{lang}.html'
# PAGE_ORDER_BY = 'basename'

DRAFT_URL = 'drafts/{slug}'
DRAFT_SAVE_AS = 'drafts/{slug}.html'
DRAFT_LANG_URL = 'drafts/{slug}-{lang}'
DRAFT_LANG_SAVE_AS = 'drafts/{slug}-{lang}.html'

# DEFAULT_PAGINATION = False
# DEFAULT_ORPHANS = 0
# PAGINATION_PATTERNS = (
#     (1, '{base_name}/', '{base_name}/index.html'),
#     (2, '{base_name}/page/{number}/', '{base_name}/page/{number}/index.html'),
# )

CATEGORY_URL = '/topic/{slug}'
CATEGORY_SAVE_AS = 'topic/{slug}.html'
CATEGORIES_SAVE_AS = 'topics.html'
CATEGORY_SUBSTITUTIONS = ()
# REVERSE_CATEGORY_ORDER = False

TAG_URL = 'tag/{slug}'
TAG_SAVE_AS = 'tag/{slug}.html'
TAGS_SAVE_AS = 'tags.html'
# TAG_SUBSTITUTIONS = ()

# AUTHOR_URL = '/author/{slug}'
AUTHOR_SAVE_AS = ''
AUTHORS_SAVE_AS = ''
# AUTHOR_SUBSTITUTIONS = ()

ARCHIVES_SAVE_AS = 'archives/index.html'
YEAR_ARCHIVE_SAVE_AS = 'archives/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'archives/{date:%Y}/{date:%b}/index.html'
# DAY_ARCHIVE_SAVE_AS = ''
# NEWEST_FIRST_ARCHIVES = True

# SLUGIFY_SOURCE = 'title'
SLUG_SUBSTITUTIONS = [('and', '')]

# OUTPUT_SOURCES = False
# OUTPUT_SOURCES_EXTENSION = '.text'
STATIC_PATHS = ['images', 'extras/robots.txt']
# STATIC_EXCLUDES = []
# STATIC_EXCLUDE_SOURCES = True
EXTRA_PATH_METADATA = {'extras/robots.txt': {'path': 'robots.txt'}}

## Caching
# CACHE_CONTENT = False
# CACHE_PATH = 'cache'
# CONTENT_CACHING_LAYER = 'reader'
# CHECK_MODIFIED_METHOD = 'mtime'
# GZIP_CACHE = True
# LOAD_CONTENT_CACHE = False

# Rendering
JINJA_ENVIRONMENT = {
    'trim_blocks': True,
    'lstrip_blocks': True,
    'extensions': ['jinja2.ext.loopcontrols',],
}
# JINJA_FILTERS = {'randomsort': randomsort}
# READERS = {}
IGNORE_FILES = ['.#*', '_*']
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {
            # 'css_class': 'highlight',
        },
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.admonition': {},
        'markdown.extensions.footnotes': {
            # 'PLACE_MARKER': '///Footnotes///',
            # 'UNIQUE_IDS': False,
            # 'BACKLINK_TEXT': '&#8617;',
        },
        'markdown.extensions.toc': {
            # 'marker': '[TOC]',
            'title': 'Contents',
            'anchorlink': True,
            # 'permalink': False,
            'baselevel': 2,
            # 'slugify' : 'markdown.extensions.headerid.slugify'
            # separator : '-'
        },
    },
    'output_format': 'html5',
}
# DOCUTILS_SETTINGS = {}
# PYGMENTS_RST_OPTIONS = []
TYPOGRIFY = True
# TYPOGRIFY_IGNORE_TAGS = []
# LOG_FILTER = [(logging.WARN, 'Feeds generated without SITEURL set properly may not be valid')]

## Categories & Tags
# USE_FOLDER_AS_CATEGORY = True
DEFAULT_CATEGORY = 'miscellany'

## Articles & Pages
# PATH_METADATA = ''
# FILENAME_METADATA = '(?P<date>d{4}-d{2}-d{2}).*'
# FORMATTED_FIELDS = ['summary']
DEFAULT_METADATA = {'Status': 'draft',}
SUMMARY_MAX_LENGTH = 50
WITH_FUTURE_DATES = False
# INTRASITE_LINK_REGEX = '[{|](?P<what>.*?)[|}]'

## Feeds
FEED_DOMAIN = SITEURL
# FEED_MAX_ITEMS

FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ATOM = None
CATEGORY_FEED_ATOM = None
TAG_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

FEED_ALL_RSS = None
FEED_RSS = None
CATEGORY_FEED_RSS = None
TAG_FEED_RSS = None
AUTHOR_FEED_RSS = None
TRANSLATION_FEED_RSS = None
# RSS_FEED_SUMMARY_ONLY = True

## Templates
# TEMPLATE_PAGES = None
DIRECT_TEMPLATES = [
    'about',
    'archives',
    'articles',
    'authors',
    'categories',
    'index',
    'search',
    'tags',
]
# PAGINATED_DIRECT_TEMPLATES = ['index']
# EXTRA_TEMPLATES_PATHS = []

## Plugins
PLUGIN_PATHS = ['plugins']
PLUGINS = [
    'jinja2content',
    'pelican-ert',
    'randomsort',
    'sitemap',
    'share_post',
    'tag_cloud',
    'tipue_search',
]

SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'weekly',
        'indexes': 'daily',
        'pages': 'monthly'
    }
}

# JINJA2CONTENT_TEMPLATES = ''

TAG_CLOUD_MAX_ITEMS = 100
TAG_CLOUD_STEPS = 5
TAG_CLOUD_SORTING = 'alphabetically'
TAG_CLOUD_BADGE = True

ERT_WPM = 300
ERT_FORMAT = '{time}'

ALWAYS_MODIFIED = True

## Theme
THEME = 'themes/anew'
THEME_STATIC_DIR = 'theme'
THEME_STATIC_PATHS = ['static']

## Theme-Specific Settings
# DISQUS_SITENAME = ''
# GITHUB_URL = ''
# GOOGLE_ANALYTICS = ''
# GA_COOKIE_DOMAIN = 'auto'
# GOSQUARED_SITENAME = ''
# MATOMO_SSL_URL = ''
# SOCIAL = ''
ABOUT_SAVE_AS = 'about.html'
ARTICLE_FANCY_FOOTNOTES = True
ARTICLE_SHARE_LINKS = True
ARTICLE_SHOW_AUTHORS = True
ARTICLE_SHOW_CATEGORY = False
ARTICLE_SHOW_DATE = True
ARTICLE_SHOW_EDITINFO = True
ARTICLE_SHOW_SUMMARY = True
ARTICLE_SHOW_TAGS = True
ARTICLE_SUMMARY_LENGTH = 300
ARTICLES_SAVE_AS = 'articles/index.html'
ARTICLES_URL = 'articles/'
ARTICLES_WIDGET_COUNT = 3
ARTICLES_WIDGET_NAME = 'More, Here'
CATEGORY_MENU_COUNT = 0
CUSTOM_AUTHOR_URL = 'ron-scott-adams'
COMMENTS_SHOW_COUNT = True
DISPLAY_PAGES_ON_MENU = True
FEEDS_LINK = SITEURL + '/' + FEED_ALL_ATOM
GITHUB_USERNAME = None
GITLAB_USERNAME = 'tohuw'
ICON_METHOD = 'js'
ICON_STYLE = 'fal'
# ISSO_URL = 'http://comments-testing.tohuw.net'
ISSO_COMMENTS_TITLE = ""
ISSO_CSS = True
ISSO_REPLY_TO_SELF = True
ISSO_REQUIRE_AUTHOR = False
ISSO_REQUIRE_EMAIL = False
ISSO_MAX_COMMENTS_TOP = 25
ISSO_MAX_COMMENTS_NESTED = 5
ISSO_REVEAL_ON_CLICK = 10
ISSO_AVATAR = False
ISSO_AVATAR_BG = "white"
ISSO_AVATAR_FG = "#51574a #447c69 #74c493 #8e8c6d #e4bf80 #e9d78e #e2975d #f19670"
ISSO_GRAVATAR = True
ISSO_VOTE = True
ISSO_VOTE_LEVELS = "-2,2"
ISSO_FEED = False
JQUERY_ENABLED = 'tipue_search' in PLUGINS or ARTICLE_FANCY_FOOTNOTES
LICENSE_TITLE = 'Your Rights & Privacy'
LICENSE_URL = SITEURL + '/rights'
LINKEDIN_USERNAME = 'tohuw'
LINKS = (
    (
        'Cormac Hogan',
        'Amazing in-depth vSAN, virtualization, & CNA coverage',
        'http://cormachogan.com'
    ),
    (
        'Live Virtually',
        'Immediately applicable virtualization notes',
        'https://livevirtually.net'
    ),
    (
        'TinkerTry',
        'Brilliant datacenter labbing & theorycrafting',
        'https://tinkertry.com'
    ),
    (
        'Virtual Ramblings',
        'Really smart & clear thoughts on storage virtualization',
        'http://thenicholson.com'
    ),
    (
        'Virtually Ghetto',
        'A virtual automation & lab genius',
        'http://virtuallyghetto.com'
    ),
    (
        'Virtually Speaking Podcast',
        'Awesome storage & availability discussions',
        'http://vspeakingpodcast.com'
    ),
    (
        'VMware Communities Roundtable',
        'All things abuzz in the VMware community',
        'http://www.talkshoe.com/talkshoe/web/talkCast.jsp?masterId=19367'
    ),
    (
        'vSphere Land',
        'A famous, long-lived, and premier virtualization resource',
        'http://vsphere-land.com'
    ),
    (
        'Yellow Bricks',
        'Virtualization insights from a Chief Technologist at VMware',
        'http://yellow-bricks.com'
    ),
    (
        'Virtualization is Life!',
        'A savvy take on VMware, Veeam, and Data Protection',
        'https://anthonyspiteri.net'
    ),
    (
        'Digital vSpace',
        'Great operations perspectives from an SDDC admin',
        'http://digitalvspace.com'
    ),
    (
        'Great White Tec',
        'Fantastically deep vSAN performance and design thoughts',
        'https://greatwhitetec.com'
    ),
    (
        'VMPete',
        'Amazingly thoughtful vSAN performance and operations articles',
        'https://vmpete.com'
    ),
    (
        'Jase\'s Place',
        'Valuable vSAN and storage knowledge from a highly experienced and capable architect',
        'http://jasemccarty.com'
    ),
    (
        'Software Defined Life',
        'An SDDC architect shares wisdom and tips on VMware and datacenter design and operations',
        'https://sddc.life'
    ),
    (
        'Blah, Cloud',
        'Savvy implementation and advice for the modern SDDC',
        'https://blah.cloud'
    ),
    (
        'Digital vSpace',
        'Great thoughts from an experienced SDDC Administrator',
        'https://www.digitalvspace.com'
    ),
    (
        'The Lone Sysadmin',
        'Benefit from the wisdom and experience of a full-stack SDDC Administrator',
        'https://lonesysadmin.net/'
    ),
    (
        'Virtual Blocks',
        'The official VMware Storage & Availability Blog',
        'https://blogs.vmware.com/virtualblocks/'
    ),
    (
        'StorageHub',
        'Official VMware Storage & Availability Info',
        'https://storagehub.vmware.com'
    ),
    (
        'ESX Virtualization',
        'A treasure trove of VMware hands-on knowledge',
        'https://www.vladan.fr'
    )
)
LINKS_URL = '/links'
LINKS_WIDGET_COUNT = 5
LINKS_WIDGET_NAME = 'More, Elsewhere'
MENUITEMS = (
    ('Articles', '/articles/', 'file-alt'),
    ('Tags', '/tags', 'tags'),
)
PAGE_SHOW_EDITINFO = True
READTIME_DISPLAY = True
SEARCH_ICON = True
SEARCH_PLACEHOLDER = 'Search'
SEARCH_SAVE_AS = 'search.html'
MODERNIZR_ENABLED = True
SITE_DESCRIPTION = '''Ron Scott-Adams is an HCI Systems Engineer with a passion for creating and
 explaining well-designed data centers and next-generation application platforms.'''
SITESUBTITLE_LINK = '/about'
SOCIAL_MENU = True
SOCIAL_QUICKLINKS = True
SOCIAL_TITLE = "Places"
TIPUE_CONTENT_LOCATION = SITEURL + '/tipuesearch_content.json'
TIPUE_SEARCH_ENABLED = 'tipue_search' in PLUGINS
TWITTER_USERNAME = 'tohuw'
