const browserSync = require('browser-sync')
const bsTunnel = 'tohuwnet'
const cloudFrontId = 'E240QHL2N4MRQU'
const cloudFrontProfile = 'tohuwnet'
const del = require('del')
const deployDest = '/var/www/tohuw/'
const deployHost = 'syndoulos.tohuw.net'
const deployUsername = 'tohuw'
const dist = 'output/'
const exec = require('child_process').exec
const glob = require('glob')
const gulp = require('gulp')
const htmlmin = require('gulp-htmlmin')
const pelicanConf = 'pelicanconf.py'
const pelicanInputDir = 'content'
const pelicanOpts = ''
const pelicanPublishConf = 'publishconf.py'
const replace = require('gulp-replace')
const rsync = require('gulp-rsync')
const s3Bucket = 'tohuwnet'
const s3Profile = cloudFrontProfile
const devServer = browserSync.create()
const watchPaths = [
  pelicanConf,
  './content/**/*.*',
  './plugins/**/*.*',
  './themes/**/static/**/*.*',
  './themes/**/templates/**/*.*'
]

// Clean
gulp.task('clean:dist', function () {
  return del(dist)
})

gulp.task('clean:junk', function (cb) {
  glob(`${dist}/**/.DS_Store+(|.gz)`, function (err, files) {
    files.forEach(function (file) {
      del([file])
    })

    cb(err)
  })

  cb()
})

gulp.task('clean:s3', function (cb) {
  return exec((`aws s3 rm s3://${s3Bucket} --profile ${s3Profile} --recursive`), function (err, stdout, stderr) {
    cb(err, stdout, stderr)
  })
})

gulp.task('clean', gulp.parallel('clean:dist'))

// Build
gulp.task('build:pelican-normal', function (cb) {
  return exec((`pelican ${pelicanInputDir} -o ${dist} -s ${pelicanConf} ${pelicanOpts}`), function (err, stdout, stderr) {
    cb(err, stdout, stderr)
  })
})

gulp.task('build:pelican-dev', function (cb) {
  return exec((`pelican -D -d ${pelicanInputDir} -o ${dist} -s ${pelicanConf} ${pelicanOpts}`), function (err, stdout, stderr) {
    cb(err, stdout, stderr)
  })
})

gulp.task('build:pelican-publish', function (cb) {
  return exec((`pelican ${pelicanInputDir} -o ${dist} -s ${pelicanPublishConf} ${pelicanOpts}`), function (err, stdout, stderr) {
    cb(err, stdout, stderr)
  })
})

// Optimize
gulp.task('build:html-optimize', function () {
  return gulp.src(dist + '**/*.html')
    .pipe(replace('&nbsp;', ' '))
    .pipe(
      htmlmin({
        collapseWhitespace: true,
        collapseInlineTagWhitespace: false,
        removeRedundantAttributes: true,
        removeEmptyAttributes: true,
        removeComments: true,
        quoteCharacter: '"',
        sortAttributes: true,
        sortClassName: true,
        useShortDoctype: true
      })
    )
    .pipe(gulp.dest(dist))
})

// Post-Build
gulp.task('build:post', gulp.parallel(
  'build:html-optimize',
  'clean:junk'
))

// Build Core Tasks
gulp.task('build:dev',
  gulp.series(
    'build:pelican-dev',
    'build:post'
  )
)

gulp.task('build:publish',
  gulp.series(
    'build:pelican-publish',
    'build:post'
  )
)

gulp.task('build',
  gulp.series(
    'build:pelican-normal',
    'build:post'
  )
)

// Publish
gulp.task('publish:rsync', function () {
  return gulp.src(dist + '**')
    .pipe(rsync({
      root: dist,
      hostname: deployHost,
      username: deployUsername,
      destination: deployDest,
      recursive: true,
      silent: true,
      compress: true,
      clean: true,
      command: false
    }))
})

gulp.task('publish:s3-uploadHtml', function (cb) {
  return exec((`aws s3 cp ${dist} s3://${s3Bucket} --profile ${s3Profile} --content-type text/html --recursive --exclude "*.*"`), function (err, stdout, stderr) {
    cb(err, stdout, stderr)
  })
})

gulp.task('publish:s3-uploadOthers', function (cb) {
  return exec((`aws s3 cp ${dist} s3://${s3Bucket} --profile ${s3Profile} --recursive --exclude "*" --include "*.*"`), function (err, stdout, stderr) {
    cb(err, stdout, stderr)
  })
})

gulp.task('publish:invalidateCFCache', function (cb) {
  return exec((`aws cloudfront create-invalidation --distribution-id ${cloudFrontId} --paths "/*" --profile ${cloudFrontProfile}`), function (err, stdout, stderr) {
    cb(err, stdout, stderr)
  })
})

gulp.task('publish:s3',
  gulp.parallel(
    'publish:s3-uploadHtml',
    'publish:s3-uploadOthers'
  )
)

gulp.task('publish',
  gulp.series(
    'clean',
    'build:publish',
    'publish:rsync'
    // 'publish:s3',
    // 'publish:invalidateCFCache'
  )
)

// Watch
gulp.task('watch:watch', function () {
  gulp.watch(watchPaths, gulp.series(
    'watch:build'
  ))
})

gulp.task('watch:build',
  gulp.series(
    'build:dev',
    function reload (cb) {
      devServer.reload()
      cb()
    }
  )
)

gulp.task('watch:serve', function () {
  devServer.init({
    open: false,
    plugins: ['bs-rewrite-rules'],
    server: {
      baseDir: dist,
      serveStaticOptions: {
        extensions: ['html']
      }
    },
    tunnel: '${bsTunnel}'
  })
})

gulp.task('watch',
  gulp.series(
    'build:dev',
    gulp.parallel(
      'watch:watch',
      'watch:serve'
    )
  )
)

// Default
gulp.task('default',
  gulp.series(
    'clean',
    'watch'
  )
)
