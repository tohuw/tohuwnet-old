"""
Jinja2 RandomSort Filter
Randomly sorts a sequence
Found on StackOverflow: https://stackoverflow.com/a/31608030/316733
"""
#!/usr/bin/env python
# -*- coding: utf-8 -*- #

import random

def randomsort(seq):
    try:
        result = list(seq)
        random.shuffle(result)
        return result
    except:
        return seq
