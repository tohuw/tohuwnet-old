"""
Jinja2 RandomSort Filter
Randomly sorts a sequence
This registers filter_randomsort.py as a Jinja2 Filter
Thanks to Kris Johnson for his article on Plugin Filters:
http://undefinedvalue.com/adding-a-jinja2-filter-with-a-pelican-plugin.html
"""
#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from pelican import signals
from . import filter_randomsort


def add_filter(pelican):
    """Add shuffle filter to Pelican"""
    pelican.env.filters.update({'randomsort': filter_randomsort.randomsort})

def register():
    """Plugin registration"""
    signals.generator_init.connect(add_filter)
